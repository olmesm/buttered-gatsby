# Buttered Gatsby

Butter CMS and Gatsby demo

## Dev

```sh
# Set node version
nvm use

# Run Dev server
yarn gatsby develop
```
